﻿#ifndef MAINVIEWM_H
#define MAINVIEWM_H

#include <QWidget>

namespace Ui {
class MainViewm;
}

class MainViewm : public QWidget
{
    Q_OBJECT

public:
    explicit MainViewm(QWidget *parent = nullptr);
    ~MainViewm();

public:
    //发射信号
    void sendZx(){
        emit zx();
    }
signals:
    void zx();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainViewm *ui;
};

#endif // MAINVIEWM_H
