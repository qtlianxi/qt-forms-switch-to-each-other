﻿#ifndef LOGINVIEW_H
#define LOGINVIEW_H

#include <QWidget>
#include <mainviewm.h>

QT_BEGIN_NAMESPACE
namespace Ui { class LoginView; }
QT_END_NAMESPACE

class LoginView : public QWidget
{
    Q_OBJECT

public:
    LoginView(QWidget *parent = nullptr);
    ~LoginView();

private slots:
    void on_pushButton_clicked();
    void zhuXiao();

private:
    Ui::LoginView *ui;
    MainViewm *mv;
};
#endif // LOGINVIEW_H
