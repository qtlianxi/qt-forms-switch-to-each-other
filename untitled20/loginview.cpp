﻿#include "loginview.h"
#include "ui_loginview.h"

LoginView::LoginView(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::LoginView)
{
    ui->setupUi(this);
    mv=new MainViewm();
    connect(mv,&MainViewm::zx,this,&LoginView::zhuXiao);
}

LoginView::~LoginView()
{
    delete ui;
}


void LoginView::on_pushButton_clicked()
{
    this->hide();

    mv->show();
}

void LoginView::zhuXiao()
{
    this->show();
    mv->hide();
}
