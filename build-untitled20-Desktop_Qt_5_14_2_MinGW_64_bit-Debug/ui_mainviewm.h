/********************************************************************************
** Form generated from reading UI file 'mainviewm.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINVIEWM_H
#define UI_MAINVIEWM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainViewm
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QPushButton *pushButton;

    void setupUi(QWidget *MainViewm)
    {
        if (MainViewm->objectName().isEmpty())
            MainViewm->setObjectName(QString::fromUtf8("MainViewm"));
        MainViewm->resize(138, 99);
        verticalLayout = new QVBoxLayout(MainViewm);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(MainViewm);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(30);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        pushButton = new QPushButton(MainViewm);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font1;
        font1.setPointSize(20);
        pushButton->setFont(font1);

        verticalLayout->addWidget(pushButton);


        retranslateUi(MainViewm);

        QMetaObject::connectSlotsByName(MainViewm);
    } // setupUi

    void retranslateUi(QWidget *MainViewm)
    {
        MainViewm->setWindowTitle(QCoreApplication::translate("MainViewm", "Form", nullptr));
        label->setText(QCoreApplication::translate("MainViewm", "\344\270\273\347\225\214\351\235\242", nullptr));
        pushButton->setText(QCoreApplication::translate("MainViewm", "\346\263\250\351\224\200", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainViewm: public Ui_MainViewm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINVIEWM_H
